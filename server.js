var express = require('express');
var server = express();

// Routes
server.get('/', function(req, res) {
  res.send('Hello World!');
});

// Listen
var port = process.env.PORT || 8080;
server.listen(port);
console.log('Listening on localhost:'+ port);